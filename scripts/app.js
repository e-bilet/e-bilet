const speed_of_bus = 1000; //ms

document.addEventListener("DOMContentLoaded", function(event) { 

    window.localStorage.clear();
    localStorage.setItem(`yourBalance`,'20');
    setTimeout(function(){ $(`#btn-enter`).prop('disabled',false); }, speed_of_bus);

});

//------------------------------------------------------------------

let bus_stop_line_1 = [
    {
     'id':'1',
     'name':'Fredry'   
    },
    {
     'id':'2',
     'name':'Raciborska'   
    },
    {
        'id':'3',
     'name':'Mickiewicza'   
    },
    {
      'id':'4',
     'name':'Kamizelka'   
    },
    {
        'id':'5',
     'name':'Rondo Powstania Styczniowego'   
    },
    {
        'id':'6',
     'name':'Wiśniewskiego'   
    },
    {
        'id':'7',
     'name':'Armii Krajowej'   
    },
    {
        'id':'8',
     'name':'Kubusia Puchatka'   
    },
    {
        'id':'9',
     'name':'Poligonowa'   
    },
    {
        'id':'10',
     'name':'Rondo Mikołaja Kopernika'   
    },
    {
        'id':'11',
     'name':'Meblowa'   
    },
    {
        'id':'12',
     'name':'Rysia'   
    },
    {
        'id':'13',
     'name':'Zachodnia'   
    },
    {
        'id':'14',
     'name':'Długa'   
    },
    {
        'id':'15',
     'name':'Uniwersytet'   
    },
    {
        'id':'16',
     'name':'Wita Stwosza'   
    },
    {
        'id':'17',
     'name':'Ogród Botaniczny'   
    },
    {
        'id':'18',
     'name':'Osiedle Przy Zalewie'   
    },
    {
        'id':'19',
     'name':'Koniec świata'   
    }
    
];

let bus_stop_line_2 = [

    {
        'id':'20',
        'name':'Niedźwiedzia'   
       },
       {
        'id':'21',
        'name':'Głogowska'   
       },
       {
        'id':'22',
        'name':'Wschodnia'   
       },
       {
        'id':'23',
        'name':'Nowy Targ'   
       },
       {
        'id':'24',
        'name':'Bema'   
       },
       {
        'id':'25',
        'name':'Poligonowa'   
       },
       {
        'id':'26',
        'name':'Kubusia Puchatka'   
       },
       {
        'id':'27',
        'name':'Warszawska'   
       },
       {
        'id':'28',
        'name':'Zagony'   
       },
       {
        'id':'29',
        'name':'Targowa'   
       },
       {
        'id':'30',
        'name':'Inowrocławska'   
       },
       {
        'id':'31',
        'name':'Brzezińska'   
       },
       {
        'id':'32',
        'name':'Gromadzka'   
       },
       {
        'id':'33',
        'name':'Krajobrazowa'   
       },
       {
        'id':'34',
        'name':'Osiniecka'   
       },
       {
        'id':'35',
        'name':'Żernicka'   
       },
       {
        'id':'36',
        'name':'Nowy Dwór'   
       },
       {
        'id':'37',
        'name':'Ogród Botaniczny'   
       },
       {
        'id':'38',
        'name':'Osiedle Przy Zalewie'   
       },
       {
        'id':'39',
        'name':'Wolska'   
       },
       {
        'id':'40',
        'name':'Długi Las'   
       },
       {
        'id':'41',
        'name':'Graniczna'
       }

];

// Append elements
for(let i = 0; i<bus_stop_line_1.length; i++){
    $(`#line1`).append(`<td class="stop-${bus_stop_line_1[i].id}">${bus_stop_line_1[i].name}</td>`)
}

for(let i = 0; i<bus_stop_line_2.length; i++){
    $(`#line2`).append(`<td class="stop-${bus_stop_line_2[i].id}">${bus_stop_line_2[i].name}</td>`)
}

//------------------------------------------------------------------

// Bus anim interval
let i = 1;
let max_bus_stop = bus_stop_line_1.length + bus_stop_line_2.length;

function busLineInterval(){
    if(i > max_bus_stop){
        i = 1;
    }

    updateBusUI(`${i}`);
    setCurrentBusStopId(`${i}`);
    i++;   
}

function setCurrentBusStopId(arg){
    localStorage.setItem('CurrentBusStopId',arg);
}

function updateBusUI(arg){
    $(document).find(`td`).removeClass('current-active');
    $(`.stop-${arg}`).addClass('current-active');
}

window.setInterval(busLineInterval, speed_of_bus);
// end -------------------------------------------------------------

function enterToBus(){
    let currenStop = localStorage.getItem(`CurrentBusStopId`);
                     localStorage.setItem(`enterID`,`${currenStop}`);
                     localStorage.setItem(`credit`,localStorage.getItem(`yourBalance`));
                     localStorage.setItem(`yourBalance`,'0');
                     updateBalanceUi();
    console.log(`Wsiadasz na przystanku nr: ${currenStop}`);
    $(`#btn-enter`).prop('disabled',true);
    $(`#btn-leave`).prop('disabled',false);
    $(`#btn-leaveFast`).prop('disabled',false);
    updateInfoUi();
}

function leaveBus(){
    let currenStop = localStorage.getItem(`CurrentBusStopId`);
                     localStorage.setItem(`LeaveID`,`${currenStop}`);
    console.log(`Wysiadasz na przystanku nr: ${currenStop}`);
    $(`#btn-leave`).prop('disabled',true);
    $(`#btn-enter`).prop('disabled',false);
    $(`#inf-end`).html(`ID: ${localStorage.getItem(`LeaveID`)}`);
    pay();
    updateInfoUi();
}


function leaveBusFast(){
    $(`#inf-payment`).html(`${localStorage.getItem('credit')} PLN`);
    localStorage.setItem(`yourBalance`,'0');
    localStorage.setItem(`credit`,'0');
    updateBalanceUi();
    updateInfoUi();
}

function pay(){

    let enterId = localStorage.getItem(`enterID`);
    let LeaveID = localStorage.getItem(`LeaveID`);

    let count = LeaveID - enterId;
    let fee = 0;

    if(count == 1){
        fee = 0.7; 
    }

    if(count == 2){
        fee = 0.6; 
    }

    if(count == 3){
        fee = 0.4; 
    }

    if(count == 4 || count == 5){
        fee = 0.2; 
    }

    if(count >=6 && count <=9){
        fee = 0.15; 
    }

    if(count >=10 && count <=13){
        fee = 0.1; 
    }

    if(count >=14 && count <=16){
        fee = 0.08; 
    }

    if(count >=17 && count <=25){
        fee = 0.04; 
    }

    if(count > 26){
        fee = 0; 
    }

    localStorage.setItem(`yourBalance`,localStorage.getItem(`credit`) - fee);
    localStorage.setItem(`credit`,'0');
    console.log(`liczba przejechanych przystanków: ${count}`);
    console.log(`opłata ${fee} zł`);

    $(`#inf-payment`).html(`${fee} PLN`);
    updateBalanceUi();

}

function updateBalanceUi(){
    $(`#yourBalance`).html(`${localStorage.getItem(`yourBalance`)} PLN`);
}

function updateInfoUi(){
    $(`#inf-credit`).html(`${localStorage.getItem(`credit`)} PLN`);
    $(`#inf-start`).html(`ID: ${localStorage.getItem(`enterID`)}`);
}